#!/usr/bin/env python3
"""Zadanie ze slajdu 32"""


class Vehicle:
    pass


class GroundVehicle(Vehicle):
    pass


class AirVehicle(Vehicle):
    pass


class Car(GroundVehicle):
    pass


class Motorcycle(GroundVehicle):
    pass


class Plane(AirVehicle):
    pass


class Rocket(AirVehicle):
    pass


class Jet(Plane):
    pass


class SpaceRocket(Rocket, Jet):
    pass


def info(information, i_obj, i_inh, i_cls):
    print(information, "is instance of:\n",
          i_inh.__name__, "->", isinstance(i_obj, i_inh), "\n",
          i_cls.__name__, "->", isinstance(i_obj, i_cls))


bmw: Car = Car()
yamaha: Motorcycle() = Motorcycle()
f16: Jet = Jet()
saturn_v = SpaceRocket()

info("BMW", bmw, GroundVehicle, Vehicle)
info("Yamaha", yamaha, GroundVehicle, Vehicle)
info("F16", f16, AirVehicle, Vehicle)
info("Saturn V", saturn_v, Rocket, Jet)
