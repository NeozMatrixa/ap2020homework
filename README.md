# AP2020HomeWork

Here I keep all home work associated with the **Academy of Python 2019/2020**.

## Getting Started

To run this files you will need working python which you can get [HERE](https://www.python.org/)

Scripts style conventions are compatible with **PEP 8**.

## Authors

![alt text][logo]
**Radosław Jachimowicz**
========================

See also the list of [contributors](https://gitlab.com/NeozMatrixa/ap2020homework/-/graphs/master) who participated in this project.

## Acknowledgments

*  [Draqun](https://github.com/Draqun/python_good_practices)
*  [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
*  [adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
*  [Zanurkuj w Pythonie](https://pl.wikibooks.org/wiki/Zanurkuj_w_Pythonie)


[logo]: https://avatars1.githubusercontent.com/u/48450331?s=100&amp;u=0f1303f8e3ddfbd6746c961de2f8a644ca50ada0&amp;v=4 "Siemanko :)"