#!/usr/bin/env python3

"""Zadanie ze slajdu 29"""


class Mammals:
    pass


class Cosmetics:
    pass


class BakedGoods:
    pass


class Carnivorous(Mammals):
    pass


class ForHair(Cosmetics):
    pass


class Cookies(BakedGoods):
    pass


domestic_cat: Carnivorous = Carnivorous()
shampoo: ForHair = ForHair()
almond: Cookies = Cookies()

print(domestic_cat)
print(shampoo)
print(almond)
