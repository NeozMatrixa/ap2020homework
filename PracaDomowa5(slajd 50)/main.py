#!/usr/bin/env python3
"""Zadanie ze slajdu 50"""


class Name:
    def __init__(self, my_name: str = "Radek", my_height: int = 186):
        self.my_name = my_name
        self.my_height = my_height


class GiveMyName(Name):
    def __init__(self):
        super(GiveMyName, self).__init__("Inny ktos", 567)


my_metric: GiveMyName = GiveMyName()
print(my_metric.my_name, my_metric.my_height)


class Core:
    def __init__(self):
        name: str = Core.__name__
        print("{} just started".format(name))

    def __del__(self):
        name: str = Core.__name__
        print("{} just died\n".format(name))


class Process(Core):
    pass


Process()


class Book:
    def __init__(self):
        self.book_name = "Raven"


class ForceBadname(Book):
    def __init__(self):
        super(ForceBadname, self).__init__()
        self.book_name = "Sea Monsters"


book = ForceBadname()
print(book.book_name)
