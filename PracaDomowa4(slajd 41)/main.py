#!/usr/bin/env python3
"""Zadanie ze slajdu 41"""


class Number:
    pass


def print_my_name() -> None:
    print("Radek")


def print_sum_of_3_numbers(num1: int, num2: int, num3: int) -> None:
    total = num1 + num2 + num3
    print(num1, "+", num2, "+", num3, "=", total)


def hi() -> str:
    return "Hello world"


def difference(num1: int, num2: int) -> int:
    return num1 - num2


def number_object() -> Number:
    return Number()


print_my_name()
print_sum_of_3_numbers(5, -3, 7)
print(hi())
print(difference(5, 2))
print(isinstance(number_object(), Number))
