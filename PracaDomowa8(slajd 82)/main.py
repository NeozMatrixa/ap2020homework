#!/usr/bin/env python3
"""Main file slajd 82"""

task1 = [
    [1],
    [2],
    [3]
]
for el in task1:
    print(el)

print("\n")

task2 = list()

for num in range(100, 115, 1):
    task2.append(num)

for el in task2:
    print(el, end=",")

task3 = [3.14, 1.61, 2.71, 4.20, 9.11]

print("\n")

for el in task3:
    print(el, end=" -> ")
