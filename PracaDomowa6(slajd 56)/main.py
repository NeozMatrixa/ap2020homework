#!/usr/bin/env python3
"""Zadanie ze slajdu 56"""


def user():
    first_name: str = input("Give me your first name: ")
    last_name: str = input("Give me your last name: ")
    age: str = input("Give me your age: ")

    print(first_name, last_name, age)


user()


def your_number():
    my_num: str = input("Give me your number: ")
    my_num: int = int(my_num)
    print(my_num, format(type(my_num).__name__))


your_number()


class Plant:
    def __init__(self, name: str, color: str):
        self.name: str = name
        self.color: str = color

    def __str__(self):
        return self.color + " " + self.name


name: str = input("Flower name: ")
color: str = input("Flower colour: ")
your_flower: Plant = Plant(name, color)
print(your_flower)
