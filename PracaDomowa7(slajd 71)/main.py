#!/usr/bin/env python3
"""main file slajd 71"""

import math


def check_name():
    """Zad 1"""
    name = input("Give me your name: ")
    print("Hello Neo :)" if name == "Radek" else f"Why are you here {name}?")


check_name()


class Triangle:
    """Triangle class"""

    def __init__(self, triangle_base: int, triangle_height: int, triangle_name: str):
        self.triangle_base: int = triangle_base
        self.triangle_height: int = triangle_height
        self.triangle_area: float = (triangle_base * triangle_height) / 2
        self.triangle_name: str = triangle_name

    def __eq__(self, other):
        return self.triangle_area == other.triangle_area

    def __lt__(self, other):
        return self.triangle_area < other.triangle_area

    def __gt__(self, other):
        return self.triangle_area > other.triangle_area

    def __le__(self, other):
        return self.triangle_area <= other.triangle_area

    def __ge__(self, other):
        return self.triangle_area >= other.triangle_area

    def __ne__(self, other):
        return self.triangle_area != other.triangle_area


a = Triangle(5, 5, "A")
b = Triangle(3, 14, "B")

print("1. A == B = {}".format(a == b))
print("2. A < B = {}".format(a < b))
print("3. A > B = {}".format(a > b))
print("4. A <= B = {}".format(a <= b))
print("5. A >= B = {}".format(a >= b))
print("6. A != B = {}\n".format(a != b))


class Cube:
    """Cube class"""

    def __init__(self, a):
        self.a = a
        self.volume = a * a * a

    def __eq__(self, other):
        return self.volume == other.volume

    def __lt__(self, other):
        return self.volume < other.volume

    def __gt__(self, other):
        return self.volume > other.volume

    def __le__(self, other):
        return self.volume <= other.volume

    def __ge__(self, other):
        return self.volume >= other.volume

    def __ne__(self, other):
        return self.volume != other.volume


cube_a = Cube(5)
cube_b = Cube(3)

print("1. A == B = {}".format(cube_a == cube_b))
print("2. A < B = {}".format(cube_a < cube_b))
print("3. A > B = {}".format(cube_a > cube_b))
print("4. A <= B = {}".format(cube_a <= cube_b))
print("5. A >= B = {}".format(cube_a >= cube_b))
print("6. A != B = {}\n".format(cube_a != cube_b))


class Vehicle:
    """Vehicle class"""

    def __init__(self, price: int):
        self.price = price

    def __eq__(self, other):
        return self.price == other.price

    def __lt__(self, other):
        return self.price < other.price

    def __gt__(self, other):
        return self.price > other.price

    def __le__(self, other):
        return self.price <= other.price

    def __ge__(self, other):
        return self.price >= other.price

    def __ne__(self, other):
        return self.price != other.price


class GroundVehicle(Vehicle):
    pass


class AirVehicle(Vehicle):
    pass


class Car(GroundVehicle):
    def __init__(self, price: int):
        super(Car, self).__init__(price)


class Motorcycle(GroundVehicle):
    def __init__(self, price: int):
        super(Motorcycle, self).__init__(price)


class Plane(AirVehicle):
    def __init__(self, price: int):
        super(Plane, self).__init__(price)


class Rocket(AirVehicle):
    def __init__(self, price: int):
        super(Rocket, self).__init__(price)


class Jet(Plane):
    def __init__(self, price: int):
        super(Jet, self).__init__(price)


class SpaceRocket(Rocket, Jet):
    def __init__(self, price: int):
        super(SpaceRocket, self).__init__(price)


volvo: Car = Car(200)
yamaha: Motorcycle = Motorcycle(100)
f14: Jet = Jet(5000)

print(f"Volvo {volvo.price}$ > Yamaha {yamaha.price}$ = " + str(volvo > yamaha))
print(f"F14 {f14.price}$ > Volvo {volvo.price}$ = " + str(f14 >= volvo))
